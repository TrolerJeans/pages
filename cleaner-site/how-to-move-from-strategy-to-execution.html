<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>How to Move from Strategy to Execution -- Troler's Personal Page :D </title>
    <link rel="shortcut icon" href="/images/toilet-paper-icon.png" type="image/x-icon">
<link rel="icon" href="toilet-paper-icon.png">
  </head>

     <a> Quick Tab: </a>
      <a href="/comics.html">Comics</a> |
      <a class=nobr href="/songs/songs.html"> Songs </a>

<p>While it is difficult to separate faulty ambition from faulty execution, we know that most companies are not great at the latter, with one study noting that <a href="/2008/06/the-secrets-to-successful-strategy-execution">employees</a> at three out of every five companies rate their organization as weak on execution. Many leaders think their strategy is “right” but lament that implementation is the problem. We have yet to meet a single leader who reports that their strategy is wrong but they are excellent at execution. Likewise, when you dig into the potential barriers to implementation, there is a general lack of understanding of the various factors at play, resulting in the inevitable managerial justifications — “poor leadership,” “inadequate talent,” “lack of process excellence,” or the still popular “culture eats strategy for breakfast.”</p>

<p>As organization designers, we are familiar with the desire of many executives to tackle execution challenges head-on, and suggest three key steps to building the right execution system:</p> 
<h2><strong>1) Achieving Strategic Clarity</strong><strong>&nbsp;</strong></h2> 
<p>The first challenge is to have the strategy that both <em>can</em> and <em>should</em> be executed. What counts as good strategy in a certain environment is hard to determine and is dependent on a <a href="https://www.bcg.com/publications/2012/your-strategy-needs-a-strategy">vast</a> number of factors, including both the industry and the competitive landscape. While the adequacy of your strategy is beyond the scope of this article, a strategy that can be executed well will match your aspirations and your <a href="/2004/06/capitalizing-on-capabilities">capabilities</a>. Therefore, two key questions to ask are:</p> 
<h3><strong>Is your strategy set at the right level of ambition?&nbsp; </strong></h3> 
<p>We can argue that there are two major errors related to strategy ambition: a type 1 error, basically inaccurate optimism, is an unrealistic or <a href="https://www.entrepreneur.com/article/300586">overly ambitious</a> strategy, which provokes a question “How can this possibly work?” and a type 2 error, irrational pessimism, is an unambitious or <a href="/2014/05/how-to-outsmart-activist-investors">overly pedestrian strategy</a> that provokes a “meh” reaction. Type 1 errors are currently more popular in the world of unicorns and activist hedge funds, but that may be just a matter of current fashion and risk appetite, not to mention the fact that such grandiose aspirations are more likely to be rewarded with big investment. Both errors are to be avoided.</p> 
<h3><strong>Do you know what capabilities are essential for your strategy? </strong></h3> 
<p>Strategic clarity requires a further step — understanding what it will take to execute the strategy. The first step is to clearly identify the essential organizational capabilities that the new strategy will require — one needs to build <a href="https://www.wealest.com/articles/business-moat">capabilities that others can’t easily emulate</a>. The second step is to know if these capabilities are something you can be expected to build. Established companies will be more conservative here than the entrepreneurs (there’s a reason Prof. Howard Stevenson <a href="/2013/01/what-is-entrepreneurship">defined entrepreneurship</a> as “the pursuit of opportunity beyond resources controlled”). One way to achieve this clarity is through a <a href="/2007/09/performing-a-project-premortem">pre-mortem</a> exercise, which allows executives to identify the soft underbelly of their strategy, and therefore their key execution risks.</p> 
<h2><strong>2) Achieving Organizational Clarity</strong></h2> 
<p>Once you select the right level of ambition and understand the key capabilities required to achieve it, you can orient your organization towards the key challenges for executing it. An operating model can provide you with a high-level model of how pursuit of your strategy will be delegated to the executive team. Three questions are helpful in building your operating model:</p> 
<h3><strong>Have you specified which unit owns which part of your strategy? </strong></h3> 
<p>While operational choices are specific to each strategy, we can cluster the key choices facing each organization around two axes that together create a <a href="https://kateskesler.com/the-organization-model-the-design-link-between-strategy-and-organization-chart/">high level set of organizational units</a>:</p> 
<ul> 
 <li>What is <strong>the key subset of strategic goals</strong>, based on your ambition, that your organization needs to achieve? How can they best be grouped? Often, complex organizations will consist of a number of different business units, grouped by geography, product type or customer type — how can you make this logic work for your strategy?</li> 
 <li>What <strong>strategic capabilities</strong> do you need that cut across these groups? How will these shared resources be grouped and managed?</li> 
</ul> 
<p>When done correctly, all of the key strategic goals and capabilities will have clear owners, reaching the “mutually exclusive, collectively exhaustive” level of clarity.</p> 
<h3><strong>Have you built the right interactions between these key units? </strong><strong>&nbsp;</strong></h3> 
<p>Once key organizational choices are made, key <strong>interdependencies</strong> need to be recognized and actively managed. Different business sub-goals will create different <strong><a href="https://en.wikipedia.org/wiki/The_Right_Fight">trade-offs</a></strong> that will need to be managed. Clear <strong>governance</strong> of how scarce resources will be divided and how shared support and capabilities will be allocated are crucial in order to align the whole organization.</p> 
<p>There is great importance in having these trade-offs proactively and explicitly managed — the whole art of the strategy will be to <a href="https://www.isc.hbs.edu/strategy/creating-a-successful-strategy/Pages/making-strategic-trade-offs.aspx">balance different subgoals</a> well to achieve a faraway goal.</p> 
<h3><strong>Have you assigned the right talent to the right roles? </strong></h3> 
<p>Your executive team will drive your operating model — your key leaders own the key sub-goals of your strategy, and they have to be up to the task.</p> 
<p>The first task is to <strong>build the right roles</strong>. Roles are just big groupings of goals, decision rights, and <a href="https://journals.sagepub.com/doi/abs/10.1177/009539978301500203">incentives</a>. The second task is to <strong>map the executive talent onto the key roles</strong>. It is better to build the operating model you need, and then check whether your current talent fits your needs. You can likely make a few risky bets, and manage with a few gaps, but if the gap is too big, you either need new talent or a new strategy.</p> 
<h2><strong>3) Building A Management System</strong></h2> 
<p>Once the strategy and operating model are set, they have to be implemented in countless day to day decisions. For that, one needs a management system. In this process, the key custodians of the execution become the <em>managers</em> — hierarchy being one key tool for an execution of complex strategy across a large number of individuals. Hierarchy is not terribly fashionable these days, partly because stories of top-down strategies gone wrong and <a href="https://psycnet.apa.org/record/2014-49512-001">leaders behaving badly</a> have <a href="https://en.wikipedia.org/wiki/On_the_Psychology_of_Military_Incompetence">became well-known</a>, leading to diminished trust in institutions of all types.</p> 
<p>We believe that new approaches to management (e.g. <a href="/2016/05/embracing-agile">agile</a>, focused on <a href="https://www.hbs.edu/faculty/Pages/item.aspx?num=41302">customer feedback</a>, with <a href="/2011/12/first-lets-fire-all-the-managers">empowered</a> employees) are an understandable reaction to a top-down command-and-control style of management. The logical expression of such an old-fashioned system is bureaucracy, which can best be described as proud obsession with form over function and process over effectiveness. In this respect, <a href="https://www.isegoria.net/2008/07/robert-conquests-three-laws-of-politics/">British historian Robert Conquest’s Third Law of Politics</a> is still correct: “The simplest way to explain the behavior of any bureaucratic organization is to assume that it is controlled by a cabal of its enemies.”</p> 
<p>However, we also believe that the idea of the demise of hierarchy and management goes <a href="/2016/07/beyond-the-holacracy-hype">too far</a> — any complex strategy will need organization and structure to achieve collective coordination. The concepts of self-organizing communities and markets, and the resulting <a href="https://cci.mit.edu/research/cidesignlab/">collective intelligence</a> they create, are the right models for many of the outcomes we care about in the world, but they’re just not good for executing complex strategies. In other words, as a general you don’t get to a Normandy landing with a bottom-up process.</p> 
<p>Our approach combines management hierarchy with two key conditions that allow for successful adaptation of strategy to the local conditions and avoidance of bureaucracy:</p> 
<h3><strong>How have you empowered your staff? </strong></h3> 
<p>Your strategy and operating model are meant to provide sufficient clarity throughout the organization on what is expected from each employee and how they can be supported to achieve what they need. Beyond that, employees will require significant latitude to adapt these goals and the approach needed to adjust to the local specifics. Good management systems explicitly ensure that there is sufficient accountability and flexibility in their design to avoid the “bureaucracy trap” and enable sufficient levels of empowerment. One example is Ritz-Carlton’s famous <a href="https://www.forbes.com/sites/micahsolomon/2021/07/06/the-2-types-of-wow-customer-service-per-zappos-and-ritz-carlton-to-be-training-for-and-empowering/?sh=50bc3cca3816">$2,000 rule</a>, which specifies that employees can spend up to that amount to make a client happy without asking for managerial approval.</p> 
<h3><strong>How have you built self-corrective feedback into your system? </strong></h3> 
<p>The importance of local context and the value of collective intelligence, customer feedback, and employee feedback imply the need to build a feedback loop that allows executives to quickly understand and react to local developments. Simply put, executives should understand what is working well and what is not, in the fastest and clearest way imaginable, and be in a position to quickly share best practices. Take the example of <a href="https://medium.com/on-human-enterprise/red-is-the-most-important-color-in-management-2bad555b95a5">Ford</a>, where a new CEO faced a culture where issues were typically not reported at the executive table, and were certainly not explored openly and addressed in depth. By instituting a business review process, the leadership team was able to focus collectively on aspects of the strategy that were not working and address them quickly and effectively, thereby creating a key link towards successful execution.</p> 
<p>In sum, collective action, i.e., large-scale cooperation, is a major <a href="https://www.smithsonianmag.com/arts-culture/what-makes-humans-different-fiction-and-cooperation-180953986/">competitive advantage of humanity</a> — to craft a fiction and make it become a reality. For that, three things have to come together: 1) a good strategy, 2) the right organization, and 3) effective management. With these three ingredients in place, human ingenuity can be unleashed, and we can collectively achieve what we do at our best — realize our dreams.</p>

<p>from https://hbr.org/2022/06/how-to-move-from-strategy-to-execution</p>

<style>
:root {
  --fg: #fff;
  --bg: #fff;
  --blue: #000;
  --white: #00ff00;
  --red: #000;
  --yellow: #000;
}
body {
  background-color: var(--bg);
  color: var(--red);
  font-family: "Lexend Exa";
  text-align: left;
  padding-top: 50px;
  line-height: 10x;
}
ul {
  list-style-type: none;
  text-align: left;
  padding: 5px 5px;
}
li {
  color: var(--white);
}
#links_container li,
a {
  color: var(--white);
  text-decoration: none;
  transition: all 0.4s;
}
#links_container li {
  font-size: 136px;
}
#links_container a:hover {
  color: var(--blue);
  transition: all 0.4s;
} </style>